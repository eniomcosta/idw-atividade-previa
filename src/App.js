
import React from 'react';
import logo from './logo.svg';
import './App.css';
import sayHello from './say_hello';


class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      message: null
    }
  }

  get_hello_message(to){
    this.setState({message: sayHello(to)});
  }

  render(){
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
            <button onClick={() => this.get_hello_message('mate')}>Clique me!</button>
            <br/>
            <label>{this.state.message}</label>
        </header>
      </div>
    );
  }
}

export default App;
